import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { GameScreenPage } from '../game-screen/game-screen';
import { AuthData } from '../../providers/auth-data';
import { GameHistoryPage} from '../game-history/game-history';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  loginPage = LoginPage;
  gameScreen = GameScreenPage;
  gameHistory = GameHistoryPage;

  constructor(public nav: NavController, public authData: AuthData, public alertCtrl : AlertController) {}

  logOut(){
    if(this.authData.IsAnonymous()){
      this.showPrompt();
    } else {
      this.authData.logoutUser().then(() => {
          this.nav.setRoot(LoginPage);
      });
    }
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Link Account with Email',
      message: "To view your work later please link your account with an email address",
      inputs: [
        {
          name: 'Email',
          placeholder: 'typuzzle@email.com',
        },
        {
          name: 'Password',
          placeholder: 'Password',
          type: "password"
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log(this.authData.IsAnonymous());
            this.authData.logoutUser().then(() => {
              this.nav.setRoot(LoginPage);
            });
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log(data.Password.length);

            if(data.Password.length >= 6){
                this.authData.ConvertAnonAccount(data.Email, data.Password);
                this.authData.logoutUser().then(() => {
                  this.nav.setRoot(LoginPage);
                  this.showSuccessAlert();
                });
              } else { this.showEmptyFieldAlert(); }
          }
        }
      ]
    });
    prompt.present();
  }

  showSuccessAlert() {
    let alert = this.alertCtrl.create({
      title: 'Account Linked Successfully',
      subTitle: 'You can now log in with your new username and password',
      buttons: ['OK']
    });
    alert.present();
  }

  //TODO: Fix validation for multiple errors

  showEmptyFieldAlert() {
    let alert = this.alertCtrl.create({
    title: 'Password Error',
    message: 'Please enter a password with more than 6 characters',
    buttons: [
      {
        text: 'Okay',
        role: 'cancel',
        handler: () => {
          this.showPrompt();

        }
      }
    ]
  });
  alert.present();
  }
}
