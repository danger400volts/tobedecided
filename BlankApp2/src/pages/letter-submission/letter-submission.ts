import { Component, } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/*
  Generated class for the LetterSubmission page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-letter-submission',
  templateUrl: 'letter-submission.html'
})
export class LetterSubmissionPage {

	toSubmit: string;
	letterValidation: FormGroup;
	submitAttempt: boolean = false;

  constructor(public navCtrl: NavController,
  	public navParams: NavParams,
  	public viewCtrl: ViewController,
  	public alertCtrl: AlertController,
  	public formBuilder: FormBuilder) {

	  	this.letterValidation = formBuilder.group({
	  		letter: ['', Validators.required],
		});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LetterSubmissionPage');
  }

  dismiss() {
  	this.viewCtrl.dismiss();
  }

  onEnter(keyCode) {
  	if (keyCode == 13) {
  		this.submit();
  	}
  }

  submit() {
  	// let data = something
  	if (this.letterValidation.valid) {
  		this.viewCtrl.dismiss(this.toSubmit);
  	}
  	else {
  		this.submitAttempt = true;
  	}
  }
}
