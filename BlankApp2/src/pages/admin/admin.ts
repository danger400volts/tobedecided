import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { NavController, NavParams, AlertController, ToastController, ActionSheetController, ModalController } from 'ionic-angular';
import { AuthData } from '../../providers/auth-data';
import { SetupWorkshopPage } from '../../pages/setup-workshop/setup-workshop';
import { Subscription } from 'rxjs';
import { GridService } from '../../providers/grid-service';
import { Workshop } from '../../models/workshop';

import firebase from 'firebase';

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html'
})

export class AdminPage {

	searchInput: string = "";
  activeWorkshops : Workshop[];
  inactiveWorkshops : Workshop[];

  workshopUpdate: Subscription;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private authData: AuthData,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public modalCtrl: ModalController,
    public gridService: GridService) {}

  ngOnDestroy() {
    this.workshopUpdate.unsubscribe();
    this.activeWorkshops = [];
    this.inactiveWorkshops = [];
  }

  ngOnInit() {
    this.workshopUpdate = this.gridService.getListOfWorkshops().subscribe(
      x => {
        function filterByActive(workshop: Workshop, isActive: boolean){
          console.log("bool" + isActive);
          if (workshop._Active == true && isActive == true) {
            return true;
          } else if (workshop._Active == false && isActive == false) {
            return true;
          }
        }
        this.activeWorkshops = x.filter(
          (workshop) => filterByActive(workshop, true)
        );
        this.inactiveWorkshops = x.filter(
          (workshop) => filterByActive(workshop, false)
        );
        console.log(this.activeWorkshops);
        console.log(this.inactiveWorkshops);
    });
  }

  onInput(ev: any) {
  	//Search logic
  }

  profile() {
    let actionSheet = this.actionSheetCtrl.create({
      title: this.authData.fireAuth.currentUser.email,
      buttons: [
        {
         text: 'Logout',
          icon: !this.platform.is('ios') ? 'log-out' : null,
          handler: () => {

            this.authData.logoutUser();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  startWorkshop() {
  	let newWorkshop = this.modalCtrl.create(SetupWorkshopPage);
  	newWorkshop.onDidDismiss(data => {
  		if (data) {
  			console.log(data);
  		}
		});
	  newWorkshop.present();
  }

  endWorkshop(WID : string){
      firebase.database().ref().child('/workshop/' + WID).update({_Active : false});
  }

  restartWorkshop(WID : string){
      firebase.database().ref().child('/workshop/' + WID).update({_Active : true});
  }
}
