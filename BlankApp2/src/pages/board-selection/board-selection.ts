import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { fiveXfiveXnine } from '../../mock-data/grids/5x5x9';
import { fiveXfiveXten } from '../../mock-data/grids/5x5x10';
import { fiveXfiveXeleven } from '../../mock-data/grids/5x5x11';
import { sevenXnineXsixteen } from '../../mock-data/grids/7x9x16';
import { GameData } from '../../models/game-data';
import { mockData } from '../../mock-data/mock-grid';


/*
  Generated class for the BoardSelection page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-board-selection',
  templateUrl: 'board-selection.html'
})
export class BoardSelectionPage {
	// Board size:5x5 = 25, 9x7 = 63

	// All the JSON parses are to get a "deep copy" of the mock grid, I was having issues with every grid being equal to the last one.
	fiveXfiveXnineGrid : GameData = JSON.parse(JSON.stringify(mockData)); 
	fiveXfiveXtenGrid : GameData = JSON.parse(JSON.stringify(mockData)); 
	fiveXfiveXelevenGrid : GameData = JSON.parse(JSON.stringify(mockData)); 
	sevenXnineXsixteenGrid : GameData = JSON.parse(JSON.stringify(mockData)); 

  constructor(public navCtrl: NavController,
  	public viewCtrl: ViewController) {
			this.fiveXfiveXnineGrid.data = fiveXfiveXnine;
			this.fiveXfiveXtenGrid.data = fiveXfiveXten;
			this.fiveXfiveXelevenGrid.data = fiveXfiveXeleven;
			this.sevenXnineXsixteenGrid.data = sevenXnineXsixteen;
			// The mock grid is 5x5 by default
			this.sevenXnineXsixteenGrid.meta.columns = 7;
			this.sevenXnineXsixteenGrid.meta.rows = 9;
		}

  ionViewDidLoad() {

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  setBoard(boardSizeX, boardSizeY, blackSquares) {

  	let data = {
			dataArray : [],
  		boardSizeX : boardSizeX,
  		boardSizeY : boardSizeY,
  		blackSquares : blackSquares
  	};

		// do grid preset assignments here
		if (boardSizeX === 7 && boardSizeY === 9){
			data.dataArray = sevenXnineXsixteen;
		} else {
			switch(blackSquares){
				case 9: data.dataArray = fiveXfiveXnine
					break;
				case 10: data.dataArray = fiveXfiveXten
					break;
				case 11: data.dataArray = fiveXfiveXeleven
					break;
				default: data.dataArray = fiveXfiveXnine
			}
		}

  	this.viewCtrl.dismiss(data);
  }

}
