import { Component } from '@angular/core';
import { ModalController, Platform, NavController, ActionSheetController, AlertController, ToastController } from 'ionic-angular';
import { GameScreenPage } from '../game-screen/game-screen';
import { BoardSelectionPage } from '../board-selection/board-selection';
import { AuthData } from '../../providers/auth-data';
import { LoginPage } from '../login/login';
import { AdminPage } from '../admin/admin';
import { GridService } from '../../providers/grid-service';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { GameData } from '../../models/game-data';
import { months } from '../../models/months';
import { Subscription } from 'rxjs';
import { TabPagePage } from '../tab-page/tab-page';
import { mockData } from '../../mock-data/mock-grid';

@Component({
  selector: 'page-game-history',
  templateUrl: 'game-history.html'
})
export class GameHistoryPage {

  games: GameData[];
  savedGames : GameData[];
  completedGames : GameData[];

  gameUpdate: Subscription;
  isAdmin: boolean = false;
  workshopGameMode : any;

  constructor(public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public platform: Platform,
    public modalCtrl: ModalController,
    public authData: AuthData,
    public gridService: GridService,
    public toastCtrl : ToastController,
    af: AngularFire
  ) {}

  ngOnInit(){
    this.gameUpdate = this.gridService.getListOfGames().subscribe(
      x => {
        let complete, saved;
        function filterByComplete(game : GameData, isComplete : boolean){
          if (game.meta.isComplete === true && isComplete === true){
            return true;
          } else if (game.meta.isComplete === false && isComplete === false) {
            return true;
          }
        }
        complete = x.slice(0, -1).filter((game) => filterByComplete(JSON.parse(game.$value), true));
        saved = x.slice(0, -1).filter((game) => filterByComplete(JSON.parse(game.$value), false));
        this.completedGames = complete.map(y => {
             return JSON.parse(y.$value);
          });
        this.savedGames = saved.map(y => {
             return JSON.parse(y.$value);
          });
    });

    this.authData.getAdmins().then(admins => {
      if (admins.includes(this.authData.GetUserUID())) {
        this.isAdmin = true;
      }
    });
  }


  ngOnDestroy() {
    //this.gameUpdate.unsubscribe();
    this.completedGames = [];
    this.savedGames = [];
  }

  //// FOR ASYNC ////
  // someAsyncOperation().then(() => {
  //       navTransition.then(() => {
  //         this.nav.pop();
  //       });

  sortArray(){
    
  }

  chooseGameMode() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'New Game',
      buttons: [
        {
         text: 'Freeplay',
          icon: !this.platform.is('ios') ? 'color-wand' : null,
          handler: () => {
            this.newGame(0, null); // Sets mode to Freeplay
            this.authData.setCurrentWorkshopStatus(false);
          }
        },
        {
          text: 'Challenge Mode',
          icon: !this.platform.is('ios') ? 'flash' : null,
          handler: () => {
            this.newGame(1, null); // Sets mode to Challenge
            this.authData.setCurrentWorkshopStatus(false);
          }
        },
        {
          text: 'Enter Workshop',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'people' : null,
          handler: () => {
            this.showWorkshopPrompt();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  formatDateTime(GameDate : any){
    // Example Date: 25th Feb 2017, 3:28:47 pm
    if (GameDate === undefined){
      return;
    }
    if (typeof(GameDate) !== 'Date'){
      GameDate = new Date(GameDate);
    }
    let date = GameDate.getDate(),
        month = GameDate.getMonth(),
        year = GameDate.getFullYear(),
        hour = GameDate.getHours(),
        minute = GameDate.getMinutes(),
        ordinal;

    if (date > 3 && date < 21){
      ordinal = 'th';
    } 
    switch (date % 10) {
        case 1:  ordinal = "st";
          break;
        case 2:  ordinal = "nd";
          break;
        case 3:  ordinal = "rd";
          break;
        default: ordinal = "th";
    }
    return '' + (date + ordinal) + ' ' + months[month] + ' ' + year + ', ' + hour + ':' + minute;
  }
  continueGame(game : GameData){
    this.navCtrl.push(GameScreenPage, game);
    console.log(game);
  }
  newGame(mode: number, WID : string) {
    let boardSelection = this.modalCtrl.create(BoardSelectionPage);
    boardSelection.onDidDismiss(data => {
      if (data) {
        let gameData: GameData
        gameData = {
          data : JSON.parse(JSON.stringify(data.dataArray)),
          meta : {
            columns: data.boardSizeX,
            rows : data.boardSizeY,
            blackSquares: data.blackSquares,
            startDate : new Date(),
            lastPlayed: new Date(),
            isChallenge: ((mode == 1) ? true : false),
            isComplete: false,
            letter: null,
            workshopID : WID,
            gameID : null
          },
          history:[]
        }
        this.navCtrl.push(GameScreenPage, gameData);
      }
    });
    boardSelection.present();
  }

  // ToDo: Redo with injected service
  profile() {
    let actionSheet = this.actionSheetCtrl.create({
      title: this.authData.fireAuth.currentUser.email,
      buttons: [
        {
         text: 'Logout',
          icon: !this.platform.is('ios') ? 'log-out' : null,
          handler: () => {

            this.authData.logoutUser();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

    showWorkshopPrompt() {
    var that = this;

    let workshopPrompt = this.alertCtrl.create({
      title: 'Want to join a workshop?',
      message: "Enter a workshop ID to join the session",
      inputs: [
        {
          name: 'workshop',
          placeholder: 'DAIUM',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {

          }
        },
        {
          text: 'Enter',
          handler: data => {
            var truefalse;
            this.authData.checkWorkshopID(data.workshop).then(res => {
            if(res._Active == true){
                this.authData.firebase.database().ref('/workshop/').once("value").then(function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        if(childSnapshot.key == data.workshop){
                          console.log("Workshop Entered");

                          that.newGame(res._GameMode, data.workshop);
                          that.authData.setCurrentWorkshopID(data.workshop);
                          that.authData.setCurrentWorkshopStatus(true);
                        }
                    });
                });
            } else {
              //console.log("Active Workshop Not Found");
              let toast = this.toastCtrl.create({
              message: 'Active Workshop Not Found',
              duration: 3000
              });
              toast.present();
          }
        return res._Active;
        });
          }
        },
      ]
    });
    workshopPrompt.present();
  }

  logOut(){
    if(this.authData.IsAnonymous()){
       //this.showPrompt();
    } else {
      this.authData.logoutUser().then(() => {
          this.navCtrl.setRoot(LoginPage);
      });
    }
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Keep your work?',
      message: "To view your work later please link your account with an email address",
      inputs: [
        {
          name: 'Email',
          placeholder: 'typuzzle@email.com',
        },
        {
          name: 'Password',
          placeholder: 'Password',
          type: "password"
        },
      ],
      buttons: [
        {
          text: 'Logout',
          role: 'destructive',
          cssClass: 'logout-button',
          handler: data => {
            console.log(this.authData.IsAnonymous());
            this.authData.logoutUser().then(() => {
              this.navCtrl.setRoot(LoginPage);
            });
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log(data.Password.length);
            if(data.Password.length >= 6){
                this.authData.ConvertAnonAccount(data.Email, data.Password);
                this.authData.logoutUser().then(() => {
                  this.navCtrl.setRoot(LoginPage);
                  this.showSuccessAlert();
                });
              } else { this.showEmptyFieldAlert(); }
          }
        }
      ]
    });
    prompt.present();
  }

  showSuccessAlert() {
    let alert = this.alertCtrl.create({
      title: 'Account Linked Successfully',
      subTitle: 'You can now log in with your new username and password',
      buttons: ['OK']
    });
    alert.present();
  }

  //TODO: Fix validation for multiple errors

  showEmptyFieldAlert() {
    let alert = this.alertCtrl.create({
    title: 'Password Error',
    message: 'Please enter a password with more than 6 characters',
    buttons: [
      {
        text: 'Okay',
        role: 'cancel',
        handler: () => {
          this.showPrompt();

        }
      }
    ]
    });
    alert.present();
  }

  pushAdminPage() {
    this.navCtrl.setRoot(TabPagePage);
  }

  public setWorkshopGameMode(GameMode : any){
      this.workshopGameMode = GameMode;
  }
}
