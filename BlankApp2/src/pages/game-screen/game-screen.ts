import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GameLogic } from '../game-component/game-component';
import { GameData } from '../../models/game-data';

@Component({
  selector: 'page-game-screen',
  templateUrl: 'game-screen.html'
})
export class GameScreenPage {
  rows : String
  data : GameData

  introText : String
  letterLabelText : String

  @ViewChild(GameData) gameData : GameData;

  constructor(public navCtrl: NavController,
  	private navParams: NavParams) {
    this.data = navParams.data;
    this.rows = this.data.meta.columns === 5 ? "five" : "seven";

    if(this.data.meta.isChallenge){
          this.introText = "Welcome to challenge mode!"
          this.letterLabelText = "Try to create this letter: ";
          this.data.meta.letter = this.getRandomLetter();
      } else {
          this.introText = "Welcome to freeplay";
          this.letterLabelText = "Design your letter and submit";
      }

  }
  onBackchat(item){ // test function
    console.log(item);
  }
  ionViewDidLoad() {
  }

  getRandomLetter(){
        var PossibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        for(var count = 0; count < 1; count++){
            return PossibleChars.charAt(Math.floor(Math.random() * PossibleChars.length));
        }
    }

}

