import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AdminPage } from '../admin/admin'
import { LettersPage } from '../letters/letters'
import { GameHistoryPage } from '../game-history/game-history';

/*
  Generated class for the TabPage tabs.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Component({
  selector: 'page-tab-page',
  templateUrl: 'tab-page.html'
})
export class TabPagePage {

  tab1Root: any = AdminPage;
  tab2Root: any = LettersPage;
  tab3Root: any = GameHistoryPage;

  constructor(public navCtrl: NavController,
  	private alertCtrl: AlertController) {

  }

  returnToGame() {
  	let alert = this.alertCtrl.create({
    title: 'TYPuzzle Admin',
    message: 'Do you wish to go back to the game screen?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.navCtrl.setRoot(this.tab3Root);
        }
      }
    ]
  });
  alert.present();
  }

}