import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import firebase from 'firebase';

/*
  Generated class for the SetupWorkshop page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-setup-workshop',
  templateUrl: 'setup-workshop.html'
})
export class SetupWorkshopPage {
	ageRange = {lower: 20, upper: 80}; // Returns object of upper and lower
	selectedMode: Number;
	workshopName: String;
  private possibleIDChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private workShopID : string;
  private workShop : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public toastCtrl : ToastController) {
        this.workShop = firebase.database().ref('/workshop');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetupWorkshopPage');
  }

  dismiss() {
  	this.viewCtrl.dismiss();
  }

  confirm() {
  	// TODO : Validation
  	let data = {
  		ageMin: this.ageRange.lower,
  		ageMax: this.ageRange.upper,
  		mode: this.selectedMode,
  		workshopName: this.workshopName
	 	};

     if(this.selectedMode == undefined){
        this.viewToast("Please Give The Workshop A Name");
     }
     else if (this.workshopName == undefined){
       this.viewToast("No Game Mode Selected");
     }
     else {
        this.createWorkshop(this.generateID());
	 	    this.viewCtrl.dismiss(data);
     }
  }

  viewToast(errorMessage : string){
      let toast = this.toastCtrl.create({
        message: errorMessage,
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
  }

  generateID(){
    this.workShopID = "";
    for(var i = 0; i < 5; i++){
      this.workShopID += this.possibleIDChars.charAt(Math.floor(Math.random() * this.possibleIDChars.length));
    }
    return this.workShopID;
  }

  createWorkshop(WID : string){
    this.workShop.child(WID).update({ _Active: true, _Name: this.workshopName, _UpperAge: this.ageRange.upper, _LowerAge: this.ageRange.lower, _GameMode: this.selectedMode}); // Add the chid of workshop with the WID as the name
  }
}
