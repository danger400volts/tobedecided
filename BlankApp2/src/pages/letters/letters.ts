import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GameData } from '../../models/game-data';
import { FilterData } from '../../models/filter-data'
import { mockData } from '../../mock-data/mock-grid';
import { GridService } from '../../providers/grid-service';
import { AngularFire } from 'angularfire2';
import { Subscription } from 'rxjs';

import { defaultFilter } from '../../mock-data/default-filter';
import { fiveXfiveXnine } from '../../mock-data/grids/5x5x9';
import { fiveXfiveXten } from '../../mock-data/grids/5x5x10';
import { fiveXfiveXeleven } from '../../mock-data/grids/5x5x11';
import { sevenXnineXsixteen } from '../../mock-data/grids/7x9x16';
import { months } from '../../models/months';
/*
  Generated class for the Letters page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-letters',
  templateUrl: 'letters.html'
})
export class LettersPage {

	fiveXfiveXnineGrid : GameData = JSON.parse(JSON.stringify(mockData)); 
	fiveXfiveXtenGrid : GameData = JSON.parse(JSON.stringify(mockData)); 
	fiveXfiveXelevenGrid : GameData = JSON.parse(JSON.stringify(mockData)); 
	sevenXnineXsixteenGrid : GameData = JSON.parse(JSON.stringify(mockData)); 

  
  Letters : Array<String> = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  filter : FilterData = defaultFilter; 
  gridTypes : any = [
    {name : "5x5_9B", selected: true, data: this.fiveXfiveXnineGrid},
    {name : "5x5_10B", selected: false, data: this.fiveXfiveXtenGrid},
    {name : "5x5_11B", selected: false, data: this.fiveXfiveXelevenGrid},
    {name : "7x9_16B", selected: false, data: this.sevenXnineXsixteenGrid}, 
  ];
  filterLetters : Array<any> = [];
  filterStartDate : any;
  filterEndDate : any;

  filterByFreeplay : boolean = true;
  filterByChallenge : boolean = true;
  completedGames : Array<GameData> = [];
  workshopGames : {workshopID : String, Games : GameData[]}[];
  challengeGames : Array<GameData>;
  filterByCapitals : boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public gridService: GridService, af: AngularFire) {
    this.fiveXfiveXnineGrid.data = fiveXfiveXnine;
    this.fiveXfiveXtenGrid.data = fiveXfiveXten;
    this.fiveXfiveXelevenGrid.data = fiveXfiveXeleven;
    this.sevenXnineXsixteenGrid.data = sevenXnineXsixteen;
    // The mock grid is 5x5 by default
    this.sevenXnineXsixteenGrid.meta.columns = 7;
    this.sevenXnineXsixteenGrid.meta.rows = 9;
    // set default
    this.filter.gridType.blackSquares = this.fiveXfiveXnineGrid.meta.blackSquares;
    this.filter.gridType.rows = this.fiveXfiveXnineGrid.meta.rows;
    this.filter.gridType.columns = this.fiveXfiveXnineGrid.meta.columns;
  }

  ionViewDidLoad() {
      //document.getElementById('gridTypeRadio1').checked = true;
  }

  ngOnInit(){
    this.filter.startDate = new Date(this.filter.endDate.getTime() - 7 * 24 * 60 * 60 * 1000);
    this.filterStartDate = this.formatDateTime(this.filter.startDate)
    this.filterEndDate = this.formatDateTime(this.filter.endDate)

    this.gridTypes[0].data.meta.blackSquares = 9;
    this.gridTypes[1].data.meta.blackSquares = 10;
    this.gridTypes[2].data.meta.blackSquares = 11;
    this.gridTypes[3].data.meta.blackSquares = 16;

    this.listGames();
  }
// ieee 830
  gameUpdate: Subscription;
  isLetterReturned(letter : String){
    let isReturned = false;
    for (let count = 0; count < this.filter.letters.length; count++){
      if (this.filter.letters[count].isReturned == true){
        if (this.filter.letters[count].letter == letter){
          isReturned = true;  
        }
      }
    }
    return isReturned;
  }
  isAnyLetterSelected(){
    let isAny = false;
    for (let count = 0; count < this.filter.letters.length; count++){
      if (this.filter.letters[count].isReturned == true){
        isAny = true;
      }
    }
    return isAny;
  }

listGames(){
    this.gridService.getListOf().forEach(user => {
      this.workshopGames = [];
      this.completedGames = [];
      for (var properties in Object.keys(user)){    // Be careful messing with this code - it took me ages
        for (var prop in user[properties]){
          if (prop[0] == '$' || prop[0] == '_'){ //Filter object irrelevant data
            continue;
          }
          let GameDataStr = (user[properties][prop]);
          // user[properties][prop]        - actual data
          let completeRegex = /"isComplete":true/;
          let checkComplete = GameDataStr.match(completeRegex);
          if (checkComplete == null){
            continue;
          }

          let gameData : GameData = JSON.parse(GameDataStr);
 
          if (this.isLetterReturned(gameData.meta.letter) == false){
            if (this.isAnyLetterSelected() == false){
            } else {
              continue;
            }
          }

          if(gameData.meta.blackSquares != this.filter.gridType.blackSquares){
            continue;
          }

          if(this.filter.workshopID != ""){
            if(gameData.meta.workshopID != this.filter.workshopID){
              continue;
            }
          }

          if(this.filter.challenge == true && this.filter.challenge == true){

          } else {
            if(gameData.meta.isChallenge != this.filter.challenge){
              continue;
            }
            if(gameData.meta.isChallenge == this.filter.freeplay){
              continue;
            }
          }

          if(this.filterByCapitals == true && gameData.meta.letter != gameData.meta.letter.toUpperCase()){
            continue;
          }

          if(this.filterByCapitals == false && gameData.meta.letter != gameData.meta.letter.toLowerCase()){
            continue;
          }
          
          if(gameData.meta.lastPlayed < this.filter.startDate || gameData.meta.lastPlayed > this.filter.endDate){
            continue;
          }

          


          //let workshopRegex = /"workshopID":null/
          //let workshopExists = GameDataStr.match(workshopRegex)
         // if(workshopExists != null){
            this.completedGames.push(JSON.parse(GameDataStr))
         // } else {
            //i//f (this.workshopGames[prop] == undefined){
             // this.workshopGames[prop] = [];
            //}
           // this.workshopGames[prop].push(JSON.parse(GameDataStr))  
         // }
        }
      }
    });
  }

  changeGridType(num : number){
    this.filter.gridType.blackSquares = this.gridTypes[num].data.meta.blackSquares;
    this.filter.gridType.columns = this.gridTypes[num].data.meta.columns;
    this.filter.gridType.rows = this.gridTypes[num].data.meta.rows;
  }
  clickCapsToggle(){
    if (this.filterByCapitals === true){
      for(var count = 0; count < this.filter.letters.length; count++){
        this.filter.letters[count].letter = this.filter.letters[count].letter.toUpperCase();
      }
    } else {
      for(var count = 0; count < this.filter.letters.length; count++){
        this.filter.letters[count].letter = this.filter.letters[count].letter.toLowerCase();
      }
    }    
  }

  searchGames(){
    this.filter.startDate = new Date(this.filterStartDate);
    this.filter.endDate = new Date(this.filterEndDate);

    //  CALL FILTER FUNCTION
    this.listGames()
    console.log(this.filter)
  }

  formatDateTime(GameDate : any){
    // Example Date: 25th Feb 2017, 3:28:47 pm
    if (GameDate === undefined){
      return;
    }
    if (typeof(GameDate) !== 'Date'){
      GameDate = new Date(GameDate);
    }
    let date = ('0' + GameDate.getDate()).slice(-2),
        month = ('0' + (GameDate.getMonth() + 1)).slice(-2),
        year = GameDate.getFullYear();
    let str = ('' + year + '-' + (month) + '-' + date + '');

    return str
  }
}
