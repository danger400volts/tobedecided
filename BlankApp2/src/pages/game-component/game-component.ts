import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GridService } from '../../providers/grid-service';
import { AuthData } from '../../providers/auth-data';
import { NavController, NavParams } from 'ionic-angular';
import { GameData } from '../../models/game-data';
import { LetterSubmissionPage } from '../letter-submission/letter-submission';

import { AlertController, ToastController, ModalController } from 'ionic-angular';

@Component({
  selector:'tile-game',
  templateUrl:'game-component.html',
  inputs: ['dataArray'],
  providers: [GridService]
})

export class GameLogic {
    // Declare possible classes and defaults
    cssClasses : { five : boolean,
                seven: boolean,
                mini : boolean,
                medium : boolean} = {
                    five: false,
                    seven: false,
                    mini: false,
                    medium: false
                }

    clickableGrid : boolean = true;
    runningHistory : boolean = false;
    skipHistory : boolean = false;
    playSpeed : number = 100; //speed of a tile switch in ms

    @Input('gameData') gameData : GameData;
    @Input('miniScale') miniScale : boolean;
    @Input('mediumScale') mediumScale : boolean;

    @Input('playHistory') playHistory : boolean = false;
    @Input('noInteraction') noInteraction : boolean = false;
    @Input('submit') submitData : Function;

    @Output() notify: EventEmitter<String> = new EventEmitter<String>();

    constructor(private importData : GridService,
        private authData : AuthData,
        private alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public navCtrl: NavController,
        public params : NavParams,
        public modalCtrl: ModalController
    ){
        this.authData.CreateGameRecord();
    }

    @Input()
    submitGame(isComplete : boolean = false){
        function submitDataToFirebase(that : any){
            if (isComplete === true) {
                that.gameData.meta.isComplete = true;
            }
            that.gameData.meta.lastPlayed = new Date();

            if(that.gameData.meta.gameID != null){
                that.authData.setisSave(true);
            } else {
                that.authData.setisSave(false);
            }

            that.gameData.meta.gameID = that.authData.generateID(that.gameData.meta.gameID);
            that.authData.SubmitData(JSON.stringify(that.gameData), that.gameData.meta.gameID);
            let toast = that.toastCtrl.create({
                message: 'Your letter was successfully saved!',
                duration: 3000
            });
            toast.present().then(function(){
                if(isComplete){ that.navCtrl.pop(); }
                
    });
        }
        if (this.gameData.meta.isChallenge === false && isComplete === true){
            let letterModal = this.modalCtrl.create(LetterSubmissionPage);
            letterModal.onDidDismiss(data => {
                if (data) {
                    this.gameData.meta.letter = data;        
                    submitDataToFirebase(this);
                }
            });
            letterModal.present();
        } else {
            submitDataToFirebase(this);
        }
    }

    clickTile(index : number){
        if (this.clickableGrid === false){
            if (this.noInteraction === true){
                if(this.runningHistory === false){
                    this.catchupFromHistory();
                }
                return;
            }
            this.skipHistory = true;
            return;
            //TODO get the catchup animation to finish if the grid is clicked
        } else {
            this.skipHistory = false;
        }
        var value = this.gameData.data[index];
        var blankIndex = undefined;

        // get the value of the tile and check its not the blank one
        if (value == 2){
            return;
        }

        blankIndex = this.adjacentBlankIndex(index);
        if (blankIndex === undefined){
            return;
        }
        this.swapTiles(index, blankIndex);
        this.writeToHistory(blankIndex);
        this.gameData.meta.lastPlayed = new Date();
    }

    swapTiles(index : number, blankIndex : number){ // 13, 18
        // update our values
        var value = this.gameData.data[index];
        this.gameData.data[blankIndex] = value;
        this.gameData.data[index] = 2; // 2 is the blank tile value

    }
    writeToHistory(blankIndex : number){
        this.gameData.history.push(blankIndex);
    }

    catchupFromHistory(){
        // WARNING do not continue to play after calling this function,
        // history will be broken
        this.runningHistory = true;
        this.clickableGrid = false;
        let historyLength : number = this.gameData.history.length
        let latestBlank : number;
        // find the current blank tile position
        for(let count = 0; count < this.gameData.data.length; count++){
            if(this.gameData.data[count] === 2){
                latestBlank = count;
            }
        }

        // the gameData.data array should already be in the lastest position,
        // we will go to the beginning, and visually display the fast forward in the playFromHistory function.
        for (let count = historyLength - 1; count >= 0; count--){
            if(count === historyLength - 1){
                this.swapTiles(this.gameData.history[count], latestBlank);
            } else {
                this.swapTiles(this.gameData.history[count], this.gameData.history[count + 1]);
            }
        }
        // Remember the tile we began at so we can return to it
        this.writeToHistory(latestBlank);
        this.playFromHistory();
    }

    playFromHistory(){
        let count = 0;
        // recursive loop (required to wait on setTimeout to finish before moving the next tile)
        function isSkippedHistory(){
            return this.skipHistory;
        }
        function loopForward(count : number, total : number, that : any) {
            function nextCount(){
                that.swapTiles(that.gameData.history[count + 1],that.gameData.history[count]);
                count++;
                if (count < total){
                    if(that.gameData.history[count + 1] !== undefined){
                        loopForward(count, total, that);
                    }
                }
            }
            if (that.skipHistory === false){
                setTimeout(function(){
                    nextCount();
                }, that.playSpeed);
            } else {
                console.log("skipped");
                nextCount();
            }
        }
        if(this.gameData.history[count + 1] !== undefined){
            loopForward(count, this.gameData.history.length, this);
        }

        if (this.noInteraction === true){
            this.clickableGrid = false;
        } else {
            this.clickableGrid = true;
        }
        this.runningHistory = false;
    }

    adjacentBlankIndex(index : number){
        try {
            let rowNum = Math.floor(index / this.gameData.meta.columns);

            if (this.gameData.data[index + 1] == 2 && Math.floor((index + 1) / this.gameData.meta.columns) === rowNum){
                return index + 1;
            } else if (this.gameData.data[index - 1] == 2 && Math.floor((index - 1) / this.gameData.meta.columns) === rowNum){
                return index - 1;
            } else if (this.gameData.data[index + this.gameData.meta.columns] == 2) {
                return index + this.gameData.meta.columns;
            } else if (this.gameData.data[index - this.gameData.meta.columns] == 2){
                return index - this.gameData.meta.columns;
            }
        } catch (error) {
            console.error(error);
        }
    }

    ngOnInit(){
        this.notify.emit('ignore');

        // although slighly overkill the IF statements enforce sizing and width.
        if (this.gameData.meta.columns === 5) {
            this.cssClasses.five = true;
            this.cssClasses.seven = false;
        } else {
            this.cssClasses.five = false;
            this.cssClasses.seven = true;
        }

        if (this.miniScale === true){
            this.cssClasses.mini = true;
            this.cssClasses.medium = false;
        } else if (this.mediumScale === true){
            this.cssClasses.mini = false;
            this.cssClasses.medium = true;
        }

        if (this.noInteraction === true){
            this.clickableGrid = false;
        }

        if (this.playHistory === true){
            this.catchupFromHistory();
        }

        if(this.gameData.history.length === 0 || this.gameData.history === undefined){
            this.gameData.history = [0];
        } else{
            //TODO: Either animate all of the users moves, or just display where they were up to.
        }
    }
}

