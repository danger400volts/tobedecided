export class GameData {
    data : Array<number>
    meta : {
        columns: number,
        rows: number,
        blackSquares: number,
        startDate : Date,
        lastPlayed: Date,
        isChallenge: Boolean,
        isComplete: Boolean,
        letter: String,
        workshopID : String
        gameID : String
    }
    history:number[]
}