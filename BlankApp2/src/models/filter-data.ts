export class FilterData {
    letters : Array<{
        letter: String, /* The array always has A-Z. The letters will either all be capitals or all be lower case, the boolean signals if it should be returned or not. */
        isReturned : boolean 
    }>
    freeplay : boolean
    challenge : boolean
    maxMovesTaken : number
    startDate : Date
    endDate : Date
    workshopID : String
    gridType : {
        columns: number,
        rows: number,
        blackSquares: number,
    }
}