import {Injectable} from '@angular/core';
import { AuthData } from './auth-data';
import { ReplaySubject } from 'rxjs';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Injectable()
export class GridService {

	public activeProject:ReplaySubject<any> = new ReplaySubject(1);

	constructor(public authData: AuthData, public af: AngularFire) {

	}

  getListOfGames(): FirebaseListObservable<any[]> {
  	return this.af.database.list('/userProfile/' + this.authData.GetUserUID());
  }

  getListOf(): FirebaseListObservable<any[]> {
  	return this.af.database.list('/userProfile/');
  }

  getListOfWorkshops(): FirebaseListObservable<any[]> {
  	return this.af.database.list('workshop');
  }
}
