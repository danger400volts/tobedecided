import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';

import firebase from 'firebase';

@Injectable()
export class AuthData {
    public fireAuth: any;
    private userProfile: any;
    private workShop : any;
    private userCredentials : any;
    public PossibleIDChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private EnteredWID : string;
    public firebase = firebase;
    private currentWorkshopID : string;
    public currentWorkshopEnabled : boolean;
    private isSave : boolean;


    constructor(public af: AngularFire) {
        this.fireAuth = firebase.auth();
        this.userProfile = firebase.database().ref('/userProfile');
        this.workShop = firebase.database().ref('/Workshop');
        this.EnteredWID = "AAAAA";
        this.isSave = false;
    }

    getAdmins(): any {
        return this.af.database.list('/admin')
            .first()
            .toPromise()
            .then(response => {
                console.log(response);
                return response.map(y => {
                    return y = y.$key;

                });
            });
    }

    loginUser(email: string, password: string): any {
        return this.fireAuth.signInWithEmailAndPassword(email, password);
    }

    signupUser(email: string, password: string): any {
        return this.fireAuth.createUserWithEmailAndPassword(email, password)
            .then((newUser) => {
                //this.userProfile.child(newUser.uid).set({ Active: true });
            });
    }

    resetPassword(email: string): any {
        return this.fireAuth.sendPasswordResetEmail(email);
    }

    IsAnonymous() : boolean {
        return firebase.auth().currentUser.isAnonymous;
    }

    logoutUser(): any {
        return this.fireAuth.signOut();
    }

    Guest() : any {
        this.fireAuth.signInAnonymously();
    }

    GetUserUID() : any {
        return firebase.auth().currentUser.uid;
    }

    CreateGameRecord(){
        this.userProfile.child(firebase.auth().currentUser.uid).update({ _Active: true });
    }

    generateID(previousGameID : string){
        var gameID;
        var PossibleIDChars = this.PossibleIDChars;

        gameID = previousGameID;
        if(previousGameID == null){
            gameID = "";
            for(var i = 0; i < 5; i++){
                gameID += PossibleIDChars.charAt(Math.floor(Math.random() * PossibleIDChars.length));
            }
        }

        return gameID;
    }

    setisSave(isSave : boolean = false){
        this.isSave = isSave;
        return isSave;
    }

    SubmitData(gameData : any, previousGameID : string) : any {
        var gameID;
        var isSave = this.isSave;
        var currentWorkshopEnabled = this.currentWorkshopEnabled;
        var currentWorkshopID = this.currentWorkshopID;

        var p1 = new Promise(
            function(resolve, reject){
                gameID = previousGameID;

                firebase.database().ref('/userProfile/' + firebase.auth().currentUser.uid).once("value").then(function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        if(childSnapshot.key == gameID){
                            if(isSave){ resolve("success"); } else { reject("same key"); }
                        } else { resolve("success");}
                    });
                });
            }
        );

        p1.then(
            function(submitData){
                var obj = {};
                obj[gameID] = gameData;
                firebase.database().ref().child('/userProfile/' + firebase.auth().currentUser.uid).update(obj);
                console.log("Data submitted to database");

                if(currentWorkshopEnabled == true){
                    console.log(currentWorkshopID);
                    var gameobj = {};
                    gameobj[gameID] = "true";
                    firebase.database().ref().child('/workshop/' + currentWorkshopID + '/' + firebase.auth().currentUser.uid).update(gameobj);
                }
            }
        ).catch(
        function(reason) {
            console.log('Handle rejected promise ('+reason+') here.');
        });
    }

    ConvertAnonAccount(email : string, password : string){
            this.userCredentials = firebase.auth.EmailAuthProvider.credential(email, password);

            firebase.auth().currentUser.link(this.userCredentials).then(function(user) {
                console.log("Anonymous account successfully upgraded", user);
            }, function(error) {
                console.log("Error upgrading anonymous account", error);
            });
    }

    checkWorkshopID(EnteredWID : string) :  any {
        this.EnteredWID = EnteredWID;

        return this.af.database.object("/workshop/" + EnteredWID)
        .first()
        .toPromise();
    }

    setCurrentWorkshopID(workshopID : string){
        this.currentWorkshopID = workshopID;
    }

    setCurrentWorkshopStatus(workshopEnabled : boolean){
        this.currentWorkshopEnabled = workshopEnabled;
    }
}
/*
        return this.af.database.object("/workshop/" + EnteredWID)
        .first()
        .toPromise()
        .then(res => {
            if(res._Active == "True"){
                console.log("Entered Workshop");
            } else { console.log("Workshop is over, sorry"); }
                return res._Active;
        });


    checkWorkshopID(EnteredWID : string){
        firebase.database().ref('/workshop/').once("value").then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                if(childSnapshot.key == EnteredWID){                    
                    firebase.database().ref('/workshop/' + EnteredWID).once("value").then(function(snapshot) {
                        snapshot.forEach(function(childSnapshot) {
                            if(childSnapshot.key == "_Active"){
                                console.log("Entered Successfully");
                                //Add userID to the correct workshop
                                //Bring the user to the game screen
                            } else { 
                                console.log("Sorry session has ended");
                            }
                        });
                    });
                } else { 
                    console.log("GET OUT OF HERE");
                    //display toast prompt error
                    //Return to history page
                    //Asked to try again
                }
            });
        });
    }
    */