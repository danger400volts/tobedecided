import {GameData} from '../models/game-data';
import {fiveXfiveXnine} from './grids/5x5x9';

export const mockData : GameData = {
    // dataArray template and default
    // 0 : White
    // 1 : Black
    // 2 : Empty
    data: fiveXfiveXnine,
    meta:{
        columns: 5,
        rows : 5,
        blackSquares: 9,
        startDate: null,
        lastPlayed: null,
        isChallenge: false,
        isComplete: false,
        letter: null,
        workshopID:null,
        gameID : null
    },
    history: []
}