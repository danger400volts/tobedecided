import {FilterData} from '../models/filter-data';
var today = new Date();
export const defaultFilter : FilterData =  {
    letters : [
        {letter:"A", isReturned:false},
        {letter:"B", isReturned:false},
        {letter:"C", isReturned:false},
        {letter:"D", isReturned:false},
        {letter:"E", isReturned:false},
        {letter:"F", isReturned:false},
        {letter:"G", isReturned:false},
        {letter:"H", isReturned:false},
        {letter:"I", isReturned:false},
        {letter:"J", isReturned:false},
        {letter:"K", isReturned:false},
        {letter:"L", isReturned:false},
        {letter:"M", isReturned:false},
        {letter:"N", isReturned:false},
        {letter:"O", isReturned:false},
        {letter:"P", isReturned:false},
        {letter:"Q", isReturned:false},
        {letter:"R", isReturned:false},
        {letter:"S", isReturned:false},
        {letter:"T", isReturned:false},
        {letter:"U", isReturned:false},
        {letter:"V", isReturned:false},
        {letter:"W", isReturned:false},
        {letter:"X", isReturned:false},
        {letter:"Y", isReturned:false},
        {letter:"Z", isReturned:false}
    ],
    freeplay : true,
    challenge : true,
    maxMovesTaken : 0,
    endDate : new Date(),
    startDate : null,
    workshopID : "",
    gridType : {
        columns : 0,
        rows : 0,
        blackSquares : 0
    }
}