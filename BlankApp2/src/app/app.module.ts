import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { GameScreenPage } from '../pages/game-screen/game-screen';
import { RegistrationPage } from '../pages/registration/registration';
import { GameHistoryPage } from '../pages/game-history/game-history'
import { BoardSelectionPage } from '../pages/board-selection/board-selection';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { SignupPage } from '../pages/signup/signup';
import { AdminPage } from '../pages/admin/admin';
import { AuthData } from '../providers/auth-data';
import { GameLogic } from '../pages/game-component/game-component';
import { GridService } from '../providers/grid-service'
import { AngularFireModule } from 'angularfire2';
import { TabPagePage } from '../pages/tab-page/tab-page';
import { LettersPage } from '../pages/letters/letters';
import { SetupWorkshopPage } from '../pages/setup-workshop/setup-workshop';
import { LetterSubmissionPage} from '../pages/letter-submission/letter-submission';

export const firebaseConfig = {
  apiKey: 'AIzaSyDsuBGZYLkS6Ajl7xX3YqLBIq1nViZG17I',
  authDomain: 'danger400volts.firebaseapp.com',
  databaseURL: 'https://danger400volts.firebaseio.com',
  storageBucket: 'danger400volts.appspot.com',
  messagingSenderId: '321635913521',
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    GameScreenPage,
    RegistrationPage,
    GameHistoryPage,
    BoardSelectionPage,
    SignupPage,
    ResetPasswordPage,
    GameLogic,
    AdminPage,
    TabPagePage,
    LettersPage,
    SetupWorkshopPage,
    LetterSubmissionPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    GameScreenPage,
    RegistrationPage,
    GameHistoryPage,
    BoardSelectionPage,
    SignupPage,
    ResetPasswordPage,
    GameLogic,
    AdminPage,
    TabPagePage,
    LettersPage,
    SetupWorkshopPage,
    LetterSubmissionPage
  ],
  providers: [
    AuthData,
    GridService
  ]
})
export class AppModule {}
