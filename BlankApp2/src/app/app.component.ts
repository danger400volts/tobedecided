import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { GameHistoryPage} from '../pages/game-history/game-history';
import { LoginPage } from '../pages/login/login';
import firebase from 'firebase';


@Component({
  template: `<ion-nav #myNav [root]="rootPage"></ion-nav>`
})
export class MyApp {
    rootPage: any;
    zone: NgZone;

  constructor(platform: Platform) {
	firebase.initializeApp({
    apiKey: "AIzaSyDsuBGZYLkS6Ajl7xX3YqLBIq1nViZG17I",
    authDomain: "danger400volts.firebaseapp.com",
    databaseURL: "https://danger400volts.firebaseio.com",
    storageBucket: "danger400volts.appspot.com",
    messagingSenderId: "321635913521"
      });

    this.zone = new NgZone({});
    firebase.auth().onAuthStateChanged((user) => {
        this.zone.run(() => {
            if (!user) {
                this.rootPage = LoginPage;
            } else {
                this.rootPage = GameHistoryPage;
            }
        });
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
